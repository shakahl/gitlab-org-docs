# Overview

The [auto-deploy process overview](auto-deploy.md) provides some details on the
building pieces of the process, a schedule overview and answers a few frequently
asked questions.

Due to the fact that this process is a shift from how the monthly release is created for self-managed customers, and is generally a disruptive workflow change, this document
aims to explain the transition plan as well as giving an overview per stakeholder in the process.

## Transition

With the existing process highlighting two special days, the 7th and 22nd of the month,
auto-deploy process will change what will no longer be the feature freeze on 2019-06-07.

| Merged date/Status | Deployed to GitLab.com? | Released with 12.0? |
| ------------------ | ----------------------- | ------------------- |
|  Merged < 2019-06-07 | :white\_check\_mark:  | :white\_check\_mark:|
|  2019-06-07 <= Merged < 2019-06-20 | :white\_check\_mark: | if on GitLab.com before 2019-06-20 |
|  2019-06-07 <= Merged < 2019-06-20 | :x: | :x: |

For any MR that is merged before 2019-06-07 and has been deployed to GitLab.com, the feature/fix will be a part of GitLab 12.0.

Any MR that is merged between 2019-06-07 and 2019-06-20 **and** has been deployed to GitLab.com during this period will be a part of GitLab 12.0.

Any MR that is merged between 2019-06-07 and 2019-06-20 but has not made it to GitLab.com (even if it is on staging environment), will not be included with GitLab 12.0.

Since the change can mean a difference between releasing something in time or missing a deadline, in the section below you can find an overview of process changes per stakeholder.

**Note** 2019-06-20 has been selected as an arbitrary cut-off date. As we see the impact of the changes in process, the date will be adjusted as needed.

## Overview per stakeholder

The change in frequency of deployments to GitLab.com impacts how the self-managed public release is created, and stakeholders in different areas:

* [Developers](#developers)
* [Product Managers](#product-managers)
* [Release Managers](#release-managers)

### Developers

Developers currently have two important dates to keep in mind, the feature freeze on the
7th and final release on the 22nd of the month.

Anything created between the 8th and the 7th falls under the **development month** with the currently active milestone, but the actual feature ends up in the release that is scheduled for the next **calendar month**.

For anything merged outside the development month that needs to end up in the final monthly release, developer applies a `Pick into XX` label. During the same period developers need to execute QA testing in one of the non-production environments after the deployment has been done. They get notified that their changes have landed in a
release candidate, and they need to manually verify the change. This means that developers work on current and next release in parallel.

The auto-deploy process changes a number of things for developers:

* The feature freeze in its current format now no longer exists.
* Any change merged into the `master` branch will be deployed to non-production environments
within several hours, and to canary within a week (in the first iteration of the process).
* The definition of done is adding a requirement that the feature is running on GitLab.com, with developer being responsible of following their feature through different environments.
* The lines between different versions of self-managed releases will be more blurry at the beginning while the process is being adopted and adapted.
* Creating smaller MRs is becoming more important because reverting changes needs to be leveraged more frequently. Feature flags can be leveraged to reduce the need for reverting.
* Priority and Severity labels are becoming more important and the task of measuring and communicating the impact is the developers responsibility.

To help explaining the change, an example chart visualises the new process. The text below the chart will attempt to explain the full process. Regular text means that there is no change, and the text in diff blocks shows the change between the current and the new process:

```patch
- We are doing this now.
+ We are doing something else.
```

Non-production environments are currently, staging.gitlab.com and pre.gitlab.com where staging is the main non-production environment (Process edge cases described in the [design document](https://about.gitlab.com/handbook/engineering/infrastructure/design/scheduled-daily-deployments/#transition-from-the-current-deployment-process) require minimum two environments). Production environments are Canary and GitLab.com.

![Feature development process](img/development.jpg)


We are starting from the point of a created MR. For most teams at this point in time, the issue is assigned to a developer, has a label `workflow::In dev`, and the developer created a MR that is not yet assigned to a reviewer.

Developer assigned a MR to a reviewer and applied the label `wokflow::In review`.

```patch
- The MR description contains one of: Closes #ISSUE, Resolves #ISSUE, Fixes #ISSUE.
+ The MR description only references the issue without a closing reference.
```

Review has been completed and the reviewer merged the MR, the developer applies the label `workflow::verification` indicating that the issue is waiting to be tested.
In the best case scenario:

```patch
- The issue is closed and the developer completed the feature.
- With first/next release candidate deployed to staging, QA issue mentions the developer and developer needs to manually verify the change on the staging server.
- Depending on when the feature is merged, feature is publicly available from few days to 3 weeks.
+ The issue remains open and the developer is responsible for seeing the feature through to production deploy.
+ Every Monday, staging environment gets deployed from the `master` branch where the developer can verify the change.
+ Depending on the overall health of the staging environment after each deploy, the feature will be deployed to the production canary environment on Wednesday the same week.
+ Developer can verify if the feature works on canary. In majority of cases, confirming that the feature works in canary and informing the Product Manager that the feature will be made available on GitLab.com is sufficient to close the assigned issue.
+ The following Monday, the feature will be available to all users when GitLab.com deployment is done using the latest successful canary deployment. Developer still needs to confirm that the feature is correctly functioning outside of canary and if it is not, issue needs to be reopened and process repeated.
```

If a bug is found in any of the environments, developer needs to work on a fix.

```patch
- A new issue is opened or an existing issue is reopened.
- Developer works on fixing the issue, creates a new MR and applies `Pick into XX` label.
- Once the MR is reviewed and merged, developer waits for the next patch release or next release candidate depending on the time of the month.
- Once the release is deployed, developer can verify the fix in staging and canary environments.
+ The issue is still open and assigned to the developer responsible for verifying the fix.
+ If the feature is under a feature flag, developer needs to turn off the feature flag. If the feature flag is on by default, developer needs to create a MR setting the feature to off.
+ In all other cases, developer needs to attempt to revert the MR in question first.
+ If the MR can be easily reverted, developer assigns the MR to a reviewer for merge and applies `Pick into auto-deploy` label.
+ If the bug caused a P1/S1 issue, developer needs to post the message in #releases channel, with the link to the revert MR, what environment was the problem discovered in (staging, canary, GitLab.com) and @ mention the active release managers. With this completed, developer can go back to repeat the process from the top.
+ In cases where a revert MR cannot be created (major framework upgrade, or an architectural change on top of which multiple features have been built), developer needs to discuss available options based on the impact the P1/S1 issue is having on all GitLab.com users. These include creating a post-deployment hot patch to reduce the impact while the real fix is created, and in some extreme cases full deployment rollback.
```

In other words, developers need to make sure that the work they did reaches GitLab.com and that they understand the impact (priority, severity) of the change they are making before the next release cut-off.
In addition to feature working correctly, documentation needs to be sufficiently updated since the change will be live before the public announcement is done.

### Product Managers

During one release cycle, Product Managers (PM) prepare the blog post text based on what was delivered by Engineering teams up until [5 working days before 22nd](https://gitlab.com/gitlab-com/www-gitlab-com/blob/901bf5a12e82f2e82c571cf9fb924dce56dde1a0/.gitlab/merge_request_templates/Release-Post.md#content-review). This includes new feature launches, breaking changes announcements and so on.

With the process transition from set dates to including only items that are running on GitLab.com, PM workflows will [require adapting too](https://gitlab.com/gitlab-com/Product/issues/199).

Changes in behaviour as well as new features will automatically reach GitLab.com users before the official release date and official announcement through the blog post.

When the feature reaches production canary environment, developer informs the PM of an imminent feature availability. If the feature is made available before the cut-off date defined earlier, the feature would also be released as part of the self-managed release.

For any feature that category PM wants to highlight, a blog post metadata (text, images notes) should be prepared in parallel with the feature going through deployments in different environments.

If a feature changes existing behaviour or a new feature needs to be made available at a certain time, PM can [leverage feature flags](https://docs.gitlab.com/11.11/ee/development/rolling_out_changes_using_feature_flags.html). Feature flag should be required before developer delivers the feature and removal of the feature flag can be done at any point in time after the feature is made available.

Alternatively, Product team might choose to leverage a different channel of communication with the wider community on GitLab.com for announcements of feature launches leaving the monthly blog post as a feature summary for self-managed customers.

### Release Managers

Release Managers (RM) are responsible for coordinating with developers in cases where a discovered bug can cause a user impact. With the auto-deploy in place, RM are responsible for pausing the auto-deploys in case of P1/S1 issues found on any of the environments.

If developer reports a P1/S1 issue found on staging, that deployment is declared unfit for production and it cannot be deployed to other environments. RMs request an immediate revert of the offending feature and ensure that the revert is a part of the next auto-deploy.

In cases where a P1/S1 issue is found on canary/production environment, RMs work with developer on applying to post-deployment patch to reduce the impact on users. Once developer prepares a solution, RM picks the change into the respective auto-deploy branch and creates a new deployment to `pre` environment where developer can verify the fix.
Once the fix is confirmed working, the RM deploys to canary/production environments.

In parallel with the ongoing active deployments, the RM remains responsible for creating any backport releases for self-managed customers per existing processes.
