# Handling omnibus releases

## Requirements

* Membership in [Release Managers group](https://gitlab.com/gitlab-org/release/managers). You should receive this as part of your [onboarding](onboarding.md).

## Process

### Create the branches

* If this is for the monthly release, first create the stable branches. For version X-X these would be
  * `X-X-stable`
  * `X-X-stable-ee`
* Create preparation branches
  * If this is for a rc name them `X-X-stable-rcY` and `X-X-stable-ee-rcY`
  * If this is for a patch release, name them `X-X-stable-patch-Y` and `X-X-stable-ee-patch-Y`

### Create the merge requests

We will need two merge requests, one for CE and one for EE.

* CE should have a source of `X-X-stable-{rc,patch}Y` and a target of `X-X-stable`
* EE should have a source of `X-X-stable--ee-{rc,patch}Y` and a target of `X-X-stable-ee`
* Set the title to `Prepare VERSION Release` or `Prepare VERSION EE Release` accordingly
* Set the milestone to the appropriate version

### Pick in the appropriate Commits

To find merge requests, use something like
```
https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests?label_name%5B%5D=Pick+into+X.X&amp;scope=all&amp;state=merged
```
Replace `X.X` with the appropriate version

To cherry pick the commits
1. Ensure you have the preparation branches checked out
1. Find the MERGE_COMMIT_SHA from the merge request. This can usually be identified by looking for the discussion point immediately after a merge that will say something like "Merge branch `foo` into master"
1. Run `git cherry-pick -m1 MERGE_COMMIT_SHA` on both preparation branches
1. Leave a note in the MR, and remove the `Pick into X.X` label
   ```
   Picked into https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests/MERGE_REQUEST_ID, will merge into `X-X-stable`, and `X-X-stable-ee`

   /unlabel ~"Pick into X.X"
   ```
1. Push both preparation branches to gitlab.com
1. Get another RM to approve and Merge the MRs once specs pass
